DOCKER_NAMESPACE ?= julienlecomte
DOCKER_IMAGE      = docker-dummy
DOCKER_TAG       ?= $(shell git rev-parse --abbrev-ref HEAD)

# TODO: faketime this crap

DOCKER_LOCAL ?= $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE)

ifeq ($(DOCKER_TAG),master)
DOCKER_TAG := latest
endif

all: local

local:
	docker build $(DOCKERFLAGS) --tag $(DOCKER_LOCAL):$(DOCKER_TAG) .

.DEFAULT_GOAL:= all
.PHONY: all local

